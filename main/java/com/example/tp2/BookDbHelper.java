package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;
import static java.sql.Types.ROWID;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "book.db";
    public static final String TABLE_NAME = "library";
    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BOOK_TITLE + " TEXT NOT NULL, "+
                COLUMN_AUTHORS + " TEXT, " +
                COLUMN_YEAR + " TEXT, " +
                COLUMN_GENRES + " TEXT, " +
                COLUMN_PUBLISHER + " TEXT, " +
                //unique vars
                "UNIQUE (" + COLUMN_BOOK_TITLE + ", " + COLUMN_AUTHORS + ") ON CONFLICT ROLLBACK);");
    }

    public void clear(){

        this.getWritableDatabase().delete(TABLE_NAME, null, null);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME + ";"); // drops the old database
        db.delete(TABLE_NAME, null, null);
        onCreate(db);

    }


    /**
     * Adds a new book
     *
     * @return true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*db.execSQL("INSERT INTO " + TABLE_NAME +
                " (" + COLUMN_BOOK_TITLE + "," + COLUMN_AUTHORS +","+ COLUMN_YEAR +","+COLUMN_GENRES +","+ COLUMN_PUBLISHER +")" +
                " VALUES ('" + book.getTitle() + "','" + book.getAuthors() + "','" + book.getYear() + "','" + book.getGenres() + "','" + book.getPublisher() + "')");
          */

        // Inserting Row
        long rowID = 0;
        ContentValues cv=new ContentValues();
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());
        // call db.insert()

        rowID = db.insert(TABLE_NAME,null,cv);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     *
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        System.out.print("mmomo");
        SQLiteDatabase db = this.getWritableDatabase();
        int res;

        // updating row
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_BOOK_TITLE,book.getTitle());
        contentValues.put(COLUMN_AUTHORS,book.getAuthors());
        contentValues.put(COLUMN_YEAR,book.getYear());
        contentValues.put(COLUMN_GENRES,book.getGenres());
        contentValues.put(COLUMN_PUBLISHER,book.getPublisher());

        res = db.update(TABLE_NAME, contentValues, _ID + " = ? ", new String[] {Long.toString(book.getId())});
        // call db.update()
        System.out.println("hhello " + book.getId());

        return res;
    }

    public Cursor fetchAllBooks() {

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {
                _ID,
                COLUMN_BOOK_TITLE,
                COLUMN_AUTHORS,
                COLUMN_YEAR,
                COLUMN_GENRES,
                COLUMN_PUBLISHER
        };
        Cursor cursor = db.query(TABLE_NAME,
                columns,
                null,
                null,
                null,
                null,
                null);


        //Cursor cursor = db.rawQuery("select * from " + TABLE_NAME,null );

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.delete(TABLE_NAME, _ID + " = ?", new String[] {String.valueOf(cursor.getLong(0))});
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {

        // build a Book object from cursor
        int id_index = cursor.getColumnIndexOrThrow(_ID);
        int title_index = cursor.getColumnIndexOrThrow(COLUMN_BOOK_TITLE);
        int authors_index = cursor.getColumnIndexOrThrow(COLUMN_AUTHORS);
        int year_index = cursor.getColumnIndexOrThrow(COLUMN_YEAR);
        int genres_index = cursor.getColumnIndexOrThrow(COLUMN_GENRES);
        int publisher_index = cursor.getColumnIndexOrThrow(COLUMN_PUBLISHER);

        long id = (Long) cursor.getLong(id_index);
        System.out.print("cursoor : " + id +" --");
        String title = (String) cursor.getString(title_index);
        String authors = (String) cursor.getString(authors_index);
        String year = (String) cursor.getString(year_index);
        String genres = (String) cursor.getString(genres_index);
        String publisher = (String) cursor.getString(publisher_index);

        return new Book(id, title, authors, year, genres, publisher);

    }
}

